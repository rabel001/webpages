((global, doc) => {
	RSACrypt = {
		getVals: () => {
			var bits = doc.getElementById('bits').options[doc.getElementById('bits').selectedIndex].value;
			var phrase = doc.getElementById('phrase1').value;
			var key = doc.getElementById('phrase2').value;
			var text = doc.getElementById('text1').value;
			return {
				"bits": bits,
				"phrase": phrase,
				"key": key,
				"text": text
			};
		},
		encryptit: () => {
			var vals = RSACrypt.getVals();
			if(vals.phrase.length === 0 || vals.key.length === 0 || vals.text.length === 0) {
				swal("Oops", "Pass phrase, key phrase, [and\\or] text area cannot be empty.", "error");
				return false;
			}
			var phraseRSA = cryptico.generateRSAKey(vals.phrase, vals.bits);
			var phraseKeyString = cryptico.publicKeyString(phraseRSA);
			var keyRSA = cryptico.generateRSAKey(vals.key, vals.bits);
			var EncryptionResult = cryptico.encrypt(vals.text, phraseKeyString, keyRSA);
			doc.getElementById('text1').value = EncryptionResult.cipher;
		},
		decryptit: () => {
			var vals = RSACrypt.getVals();
			if(vals.phrase.length === 0 || vals.text.length === 0) {
				swal("Oops", "Pass phrase [and\\or] text area cannot be empty.", "error");
				return false;
			}
			var phraseRSA = cryptico.generateRSAKey(vals.phrase, vals.bits);
			var cipher = vals.text;
			doc.getElementById('text1').value = cryptico.decrypt(cipher, phraseRSA).plaintext;
		},
		saveFile: () => {
			var a = doc.createElement('a');
			a.id = '_download';
			var filename = doc.getElementById('OpenFile').getAttribute("ref");
			//if (filename === null) return;
			if(doc.getElementById('text1').value === '' || doc.getElementById('text1').value == null) return;
			var text = doc.getElementById('text1').value;
			var lines = null;
			var allLines = text.split(/\r\n|\n/);
			allLines.map((line) => {
				lines = (lines === null) ? line + '\r\n' : lines + line + '\r\n';
			});
			lines = lines.trim();
			var blob = new Blob([lines], {
				type: "text/plain;charset=UTF-8"
			});
			var url = window.URL.createObjectURL(blob);
			url = window.URL.createObjectURL(blob);
			a.href = url;
			if(doc.getElementById('OpenFile').getAttribute("ref") !== null){
				filename = doc.getElementById('OpenFile').getAttribute("ref");
			}
			if(filename === null) filename = 'RSACrypt_Export.txt';
			a.download = filename;
			doc.getElementById('OpenFile').appendChild(a);
			a.click();
		},
		toggleSaveBtn: function() {
			if(doc.getElementById('text1').value !== '')
			{
				doc.getElementById('save').disabled = false;
			}
			else {
				doc.getElementById('save').disabled = true;
			}
		},
		OpenFile: () => {
			doc.getElementById('OpenFile').click();
		},
		handleOpenFile: (evt) => {
			var reader = new FileReader();
			reader.onload = (event) => {
				if(doc.getElementById('text1').value.length !== 0) {
					doc.getElementById('text1').value = '';
				}
				var file = event.target.result;
				var allLines = file.split(/\r\n|\n/);
				allLines.map((line) => {
					doc.getElementById('text1').value += line + '\n';
				});
				RSACrypt.toggleSaveBtn();
			};
			reader.onerror = (evt) => {
				alert(evt.target.error.name);
			};
			reader.readAsText(evt.target.files[0]);
			doc.getElementById('OpenFile').setAttribute("ref", evt.target.files[0].name);
		}
	};
	window.addEventListener('load', function() {
		doc.getElementById('OpenFile').addEventListener('change', RSACrypt.handleOpenFile, false);
		doc.getElementById('text1').addEventListener("paste",function() {
			RSACrypt.toggleSaveBtn();
		});
		doc.getElementById('text1').addEventListener('input', function() {
			RSACrypt.toggleSaveBtn();
		});
		doc.getElementById('text1').addEventListener('change', function() {
			RSACrypt.toggleSaveBtn();
		});
	});
	global['RSACrypt'] = RSACrypt;
})(window, document);