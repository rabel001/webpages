((global, doc) => {
	//cryptpage3.js
	//This script only works with encryption3.html
	var _gib = {
		getPwds: () => {
			var _pass1 = doc.getElementById('_pass1').value;
			var _pass2 = doc.getElementById('_pass2').value;
			var _state = true;
			if (_pass1 !== _pass2) {
				_state = false;
			}
			return {
				"_pass1": _pass1,
				"_pass1": _pass2,
				"_state": _state
			}
		},
		encryptText: () => {
			if (_gib.IsEncrypted) return;
			if (doc.getElementById('_cpylbl').innerText) {
				doc.getElementById('_cpylbl').innerText = null;
			}
			var arr = _gib.getPwds();
			if (!arr._state) {
				alert('Passwords do not match.');
				doc.getElementById('_cpylbl').innerText = 'Passwords do not match.';
				return;
			}
			var _textarea = doc.getElementById('_txtarea');
			_inputText = _textarea.value;
			var _encrypted = gibberish_helpers.encrypt(_inputText, arr._pass1);
			_textarea.value = _encrypted;
			global._gib.IsEncrypted = true;
			var crypty = JSON.parse(_encrypted);
			document.querySelector('#nonce').value = crypty.nonce;
			document.querySelector('#salt').value = crypty.salt;
			document.querySelector('#keylen').value = crypty.keylen;
			document.querySelector('#scryptM').value = crypty.scryptM;
			document.querySelector('#scryptO').value = crypty.scryptO;
			document.querySelector('#cipherText').value = crypty.cipherText;
			document.querySelector('details').style.display = "block";
		},
		decryptText: () => {
			if (doc.getElementById('_cpylbl').innerText) {
				doc.getElementById('_cpylbl').innerText = null;
			}
			var arr = _gib.getPwds();
			if (!arr._state) {
				alert('Passwords do not match.');
				doc.getElementById('_cpylbl').innerText = 'Passwords do not match.';
				return;
			}
			var _textarea = doc.getElementById('_txtarea');
			var _encrypted = _textarea.value;
			_textarea.value = gibberish_helpers.decrypt(_encrypted, arr._pass1);
			document.querySelector('details').style.display = "none";
			_gib.IsEncrypted = false;
		},
		openFile: () => {
			doc.getElementById('_hiddenFile').click();
			document.querySelector('details').style.display = "none";
			_gib.IsEncrypted = false;
		},
		saveFile: () => {
			var a = doc.createElement('a');
			a.id = '_download';
			var filename = doc.getElementById('_hiddenFile').getAttribute("ref");
			//if (filename === null) return;
			if (doc.getElementById('_txtarea').value == '' || doc.getElementById('_txtarea').value == null) return;
			var text = doc.getElementById('_txtarea').value;
			var lines = null;
			var allLines = text.split(/\r\n|\n/);
			allLines.map((line) => {
				if (lines === null) {
					lines = line + '\r\n';
				} else {
					lines += line + '\r\n';
				}
			});
			lines = lines.trim();
			var blob = new Blob([lines], {
				type: "text/plain;charset=UTF-8"
			});
			var url = window.URL.createObjectURL(blob);
			url = window.URL.createObjectURL(blob);
			a.href = url;
			if (filename === null) filename = 'rename_me.txt';
			a.download = filename;
			doc.getElementById('_hiddenFile').appendChild(a);
			a.click();
		},
		handleOpenFile: (evt) => {
			doc.getElementById('_txtarea').value = null;
			global._gib.file = evt.target.files[0];
			if (global._gib.file === undefined) return;
			if (!/text\/plain/.test(global._gib.file.type) && global._gib.file.type !== '') {
				alert(`Wrong file type: '${global._gib.file.type}'.\nFile type must be: 'text/plain'.`);
				return;
			}
			var reader = new FileReader();
			reader.onload = (event) => {
				var iFile = event.target.result;
				var allLines = iFile.split(/\r\n|\n/);
				allLines.map((line) => {
					doc.getElementById('_txtarea').value += line + '\n';
				});
			};
			reader.onerror = (evt) => {
				alert(evt.target.error.name);
			};
			reader.readAsText(global._gib.file);
			doc.getElementById('_hiddenFile').setAttribute("ref", global._gib.file.name);
			doc.getElementById('_save').disabled = false;
		}
	}
	global['_gib'] = _gib;
	global._gib.IsEncrypted = false;
	global.addEventListener('load', function () {
		doc.getElementById('_hiddenFile').addEventListener('change', _gib.handleOpenFile, false);
	});
})(window, document);