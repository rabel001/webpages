jQuery(() => {
	$('form').on('paste', 'input, textarea', (e) => {
		setTimeout(() => {
			if ($(e.currentTarget).val().length === 0) {
				e.currentTarget.parentElement.classList.remove('notEmpty');
			}
			if ($(e.currentTarget).val().length > 0) {
				e.currentTarget.parentElement.classList.add('notEmpty');
			}
		}, 0);
	});
	$('form').on('cut', 'input, textarea', (e) => {
		setTimeout(() => {
			if ($(e.currentTarget).val().length === 0) {
				e.currentTarget.parentElement.classList.remove('notEmpty');
			}
			if ($(e.currentTarget).val().length > 0) {
				e.currentTarget.parentElement.classList.add('notEmpty');
			}
		}, 0);
	});
	$('form').on("keyup", 'input, textarea', (e) => {
		if ($(e.currentTarget).val().length === 0) {
			e.currentTarget.parentElement.classList.remove('notEmpty');
		} else if ($(e.currentTarget).val().length > 0) {
			e.currentTarget.parentElement.classList.add('notEmpty');
		}
	});
	$('#_txtarea').change((evt) => {
		var input = $(this);
		input.parent().toggleClass('notEmpty', !!input.val());
	});
});

(function (global, doc) {
	//cryptpage2.js
	//This script only works with encryption2.html
	var _gib = {
		getPwds: () => {
			var _pass1 = doc.getElementById('_pass1').value;
			var _pass2 = doc.getElementById('_pass2').value;
			var _state = true;
			if (_pass1 !== _pass2) {
				_state = false;
			}
			return {
				"_pass1": _pass1,
				"_pass2": _pass2,
				"_state": _state
			}
		},
		encryptText: () => {
			if (_gib.IsEncrypted) return;
			if (doc.getElementById('_cpylbl').innerText) {
				doc.getElementById('_cpylbl').innerText = null;
			}
			var arr = _gib.getPwds();
			if (!arr._state) {
				alert('Passwords do not match.');
				doc.getElementById('_cpylbl').innerText = 'Passwords do not match.';
				return;
			}
			var _textarea = doc.getElementById('_txtarea');
			_inputText = _textarea.value;
			var _encrypted = gibberish_helpers.encrypt(_inputText, arr._pass1);
			_textarea.value = _encrypted;
			global._gib.IsEncrypted = true;
		},
		decryptText: () => {
			if (doc.getElementById('_cpylbl').innerText) {
				doc.getElementById('_cpylbl').innerText = null;
			}
			var arr = _gib.getPwds();
			if (!arr._state) {
				alert('Passwords do not match.');
				doc.getElementById('_cpylbl').innerText = 'Passwords do not match.';
				return;
			}
			var _textarea = doc.getElementById('_txtarea');
			var _encrypted = _textarea.value;
			_textarea.value = gibberish_helpers.decrypt(_encrypted, arr._pass1);
			_gib.IsEncrypted = false;
		},
		openFile: () => {
			doc.getElementById('_hiddenFile').click();
			_gib.IsEncrypted = false;
		},
		saveFile: () => {
			var a = doc.createElement('a');
			a.id = '_download';
			var filename = doc.getElementById('_hiddenFile').getAttribute("ref");
			//if (filename === null) return;
			if (doc.getElementById('_txtarea').value == '' || doc.getElementById('_txtarea').value == null) return;
			var text = doc.getElementById('_txtarea').value;
			var lines = null;
			var allLines = text.split(/\r\n|\n/);
			allLines.map((line) => {
				if (lines === null) {
					lines = line + '\r\n';
				} else {
					lines += line + '\r\n';
				}
			});
			lines = lines.trim();
			var blob = new Blob([lines], {
				type: "text/plain;charset=UTF-8"
			});
			var url = window.URL.createObjectURL(blob);
			url = window.URL.createObjectURL(blob);
			a.href = url;
			if (filename === null) filename = 'rename_me.txt';
			a.download = filename;
			doc.getElementById('_hiddenFile').appendChild(a);
			a.click();
		},
		handleOpenFile: (evt) => {
			doc.getElementById('_txtarea').value = null;
			global._gib.file = evt.target.files[0];
			if (global._gib.file === undefined) return;
			if (!/text\/plain/.test(global._gib.file.type) && global._gib.file.type !== '') {
				alert(`Wrong file type: '${global._gib.file.type}'.\nFile type must be: 'text/plain'.`);
				return;
			}
			var reader = new FileReader();
			reader.onload = (event) => {
				var iFile = event.target.result;
				var allLines = iFile.split(/\r\n|\n/);
				allLines.map((line) => {
					doc.getElementById('_txtarea').value += line + '\n';
				});
				$('#_txtarea').change();
			};
			reader.onerror = (evt) => {
				alert(evt.target.error.name);
			};
			reader.readAsText(global._gib.file);
			doc.getElementById('_hiddenFile').setAttribute("ref", global._gib.file.name);
			doc.getElementById('_save').disabled = false;
		}
	}
	global['_gib'] = _gib;
	global._gib.IsEncrypted = false;
	global.addEventListener('load', function () {
		doc.getElementById('_hiddenFile').addEventListener('change', _gib.handleOpenFile, false);
	});
})(window, document);
